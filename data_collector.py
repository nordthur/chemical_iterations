from openpyxl import load_workbook

probe_number = "65.22-2-1"
company_name = 'ГМЗ "Куликово поле"'


class ExcelTab:
    def __init__(self, tabname):
        self.excel_tabname = tabname

    def tabname_reader(self):
        excel_file = load_workbook("Рабочий журнал по ингредиентам 2022.xlsx", data_only=True)
        return excel_file[self.excel_tabname]


tab_list = {
    'tab_miscellaneous': ExcelTab("Разное"),
    'tab_nitrogen': ExcelTab("Группа азота"),
    'tab_metals': ExcelTab("Металлы"),
    'tab_organic': ExcelTab("Органика"),
    'tab_phys': ExcelTab("Физ.свойства")
}

miscellaneous_tabname = tab_list['tab_miscellaneous'].tabname_reader()
nitrogen_tabname = tab_list['tab_nitrogen'].tabname_reader()
metals_tabname = tab_list['tab_metals'].tabname_reader()
organic_tabname = tab_list['tab_organic'].tabname_reader()
phys_tabname = tab_list['tab_phys'].tabname_reader()


class CompanyName:
    def __init__(self, tabname):
        self.tabname = tabname

    # Returns the Row of the selected probe
    def trial_number_index(self, probe):
        search_starter = 5  # It is equal to A:5
        max_row_length = self.tabname.max_row
        cells = self.tabname
        trial_dates_array = []

        for cell in range(search_starter, max_row_length):
            trial_dates_array.append(cells.cell(cell, 3).value)
        trial_date_index = trial_dates_array.index(probe) + 5
        return trial_date_index

    # Returns the list of company name with all probes, listed in a row
    def client_company_set(self):
        start_probe_index = self.trial_number_index(probe_number)
        initial_company_name = self.tabname.cell(start_probe_index, 2).value
        iteration_company_name = self.tabname.cell(start_probe_index, 2).value
        probes_per_company = {}

        while iteration_company_name == initial_company_name:
            iteration_company_name = self.tabname.cell(start_probe_index, 2).value
            iteration_index_name = self.tabname.cell(start_probe_index, 3).value
            if iteration_company_name is None:
                break
            probes_per_company[str(iteration_index_name)] = iteration_company_name
            start_probe_index = start_probe_index + 1

            if iteration_company_name != initial_company_name:  # and iteration_company_name is not None:
                probes_per_company.pop(iteration_index_name)

        return probes_per_company

    # Aid for ingredients_errors_concentration function
    def ingredients_tab_list(self):
        cell_e_col = 5
        cell_e_row = 2
        max_col_width = self.tabname.max_column + 1
        iteration_cell = self.tabname
        current_ingredient_name = None
        ingredients_list = {}
        ingredient_name_start_index = {}

        for column in range(cell_e_col, max_col_width):
            ingredient_name = iteration_cell.cell(cell_e_row, column).value

            if ingredient_name is not None:
                current_ingredient_name = ingredient_name

            if current_ingredient_name in ingredients_list:
                ingredients_list[current_ingredient_name] = ingredients_list[current_ingredient_name] + 1
            else:
                ingredients_list[current_ingredient_name] = 1
        for column in range(cell_e_col, max_col_width):
            ingredient_name = iteration_cell.cell(cell_e_row, column).value
            if ingredient_name is not None:
                ingredient_name_start_index[ingredient_name] = iteration_cell.cell(cell_e_row, column)

        return ingredients_list, ingredient_name_start_index

    # Returns "Id", "ingredient_name", "key_params"(first, second), "key_params_cells"(first, second)
    def ingredients_params(self):
        tab_selector = self.tabname
        ingredients_and_cells_count = self.ingredients_tab_list()[0]
        most_recent_cell = 5
        list_of_ingredients = []
        item_number = 0

        for item in ingredients_and_cells_count:
            iteration_range = ingredients_and_cells_count[item] + 1
            temp_item = {
                'probe_number': '',
                'id': item_number,
                'ingredient_name': item,
                'params': {
                },
                'key_params': {
                    'first_param': '',
                    'second_param': ''
                },
                'key_params_row_cell': {
                    'first_param_cell': '',
                    'second_param_cell': ''
                },
                'key_params_values': {}
            }
            list_of_ingredients.append(temp_item)
            for cell in range(1, iteration_range):
                list_of_ingredients[item_number]['params'][str(cell)] = tab_selector.cell(3, most_recent_cell).value

                if cell == iteration_range - 1:
                    value = tab_selector.cell(3, most_recent_cell)
                    list_of_ingredients[item_number]['key_params_row_cell']['second_param_cell'] = value
                elif cell == iteration_range - 2:
                    value = tab_selector.cell(3, most_recent_cell)
                    list_of_ingredients[item_number]['key_params_row_cell']['first_param_cell'] = value
                most_recent_cell = most_recent_cell + 1

            item_number = item_number + 1

        for item in list_of_ingredients:
            if len(item['params']) == 2:
                item['key_params']['second_param'] = item['params']['2']
                item['key_params']['first_param'] = item['params']['1']
            elif len(item['params']) == 1:
                item['params']['1'] = item['ingredient_name']
                item['key_params']['first_param'] = item['params']['1']
                item['key_params_row_cell']['first_param_cell'] = item['key_params_row_cell']['second_param_cell']
                item['key_params'].pop('second_param')
                item['key_params_row_cell'].pop('second_param_cell')
            else:
                params_length = len(item['params'])
                last_param = params_length
                penultimate_param = params_length - 1
                item['key_params']['second_param'] = item['params'][f"{last_param}"]
                item['key_params']['first_param'] = item['params'][f"{penultimate_param}"]

        return list_of_ingredients

        # for column in range(5, max_col_width):
        #     print(tab_selector.cell(3, column))
        #     print(tab_selector.cell(3, column).value)

    # Calculates all the probe numbers with all of their properties (trial date, water category, indexes, etc)
    def probe_ingredients_collector(self, probe=probe_number):
        tab_selector = self.tabname
        row_finder = self.trial_number_index
        companies_in_probes = self.client_company_set()
        trial_date_row_cell = row_finder(probe)
        ingredients_list = []

        # Loop for finding all rows numbers of a company in a row
        for probe_row_num in companies_in_probes:
            ingredients_params = self.ingredients_params()
            current_probe_row = row_finder(probe_row_num)
            for ingredient in ingredients_params:
                ingredient['probe_row'] = current_probe_row
                ingredient['probe_number'] = tab_selector.cell(current_probe_row, 3).value
                ingredient['date_of_trial'] = tab_selector.cell(trial_date_row_cell, 1).value
                ingredient['water_category'] = tab_selector.cell(current_probe_row, 4).value

                if len(ingredient['key_params']) == 1:
                    first_column_cell = ingredient['key_params_row_cell']['first_param_cell'].column
                    ingredient['key_params_values']['first_value'] = \
                        tab_selector.cell(current_probe_row, first_column_cell).value
                    ingredients_list.append(ingredient)
                else:
                    first_column_cell = ingredient['key_params_row_cell']['first_param_cell'].column
                    second_column_cell = ingredient['key_params_row_cell']['second_param_cell'].column
                    ingredient['key_params_values']['first_value'] = \
                        tab_selector.cell(current_probe_row, first_column_cell).value
                    ingredient['key_params_values']['second_value'] = \
                        tab_selector.cell(current_probe_row, second_column_cell).value
                    ingredients_list.append(ingredient)

        return ingredients_list

    def probe_exporter(self):
        probes_set = self.client_company_set()
        return probes_set

    @staticmethod
    def all_tabs_probes_collector():
        all_tabs_list = []
        for tab in tab_list:
            all_tabs_list.append(CompanyName(tab_list[tab].tabname_reader()).probe_ingredients_collector())
        return all_tabs_list

#     def probes_set_exporter(self):
#         probes_set = self.client_company_set()
#         return probes_set
#
#
# client_company_set = CompanyName.probes_set_exporter(CompanyName)
# print(client_company_set)

# Uncomment when in use
# func_runner = CompanyName(phys_tabname)
# print(func_runner.probes_set_expoter())
