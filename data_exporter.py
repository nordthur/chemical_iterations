from openpyxl import load_workbook
from data_collector import CompanyName, company_name


class ExcelTabWriter:
    def __init__(self):
        self.collector_doc = load_workbook("Рабочий журнал по ингредиентам 2022.xlsx", data_only=True)
        self.current_doc = load_workbook('Протокол вода.xlsx')
        self.current_tab = self.current_doc['Протокол']
        self.ingredient_tab = self.current_doc['Ингредиенты']

    @staticmethod
    def list_of_excl_nones():
        exported_list_caller = CompanyName.all_tabs_probes_collector()
        sorted_list = []
        for tab in exported_list_caller:
            for probe_ingredients_per_company_per_tab in tab:
                # probe_number = probe_ingredients_per_company_per_tab['probe_number']
                # ingredient_name = probe_ingredients_per_company_per_tab['ingredient_name']
                # water_category = probe_ingredients_per_company_per_tab['water_category']
                # trial_date = probe_ingredients_per_company_per_tab['date_of_trial']
                # print(probe_ingredients_per_company_per_tab)
                if len(probe_ingredients_per_company_per_tab['key_params_values']) == 1:
                    if probe_ingredients_per_company_per_tab['key_params_values']['first_value'] is None:
                        probe_ingredients_per_company_per_tab.clear()
                else:
                    if probe_ingredients_per_company_per_tab['key_params_values']['first_value'] is None \
                            and probe_ingredients_per_company_per_tab['key_params_values']['second_value'] is None:
                        probe_ingredients_per_company_per_tab.clear()
                sorted_list.append(probe_ingredients_per_company_per_tab)
        return sorted_list

    @staticmethod
    def empty_elems_remover(self):
        list_excl_nones = self.list_of_excl_nones()
        clean_elems = []

        for elem in list_excl_nones:
            if len(elem) != 0:
                clean_elems.append(elem)

        essential_values = []

        for elem in clean_elems:
            elem.pop('params')
            elem.pop('key_params_row_cell')
            elem.pop('probe_row')
            essential_values.append(elem)

        return essential_values

    def unique_probes_list(self):
        imported_list = self.empty_elems_remover(ExcelTabWriter)
        unique_probe_set = {}
        for elem in imported_list:
            if elem['probe_number'] not in unique_probe_set:
                unique_probe_set[elem['probe_number']] = elem['probe_number']

        return unique_probe_set

    @staticmethod
    def sorter(self):
        array_of_probe_details = self.empty_elems_remover(ExcelTabWriter)
        unique_probes_set = self.unique_probes_list(ExcelTabWriter)
        container = []

        for probe in unique_probes_set:
            probe_details_container = []
            temp_elem = {
                'probe_number': '',
                'trial_date': '',
                'water_category': '',
                'ingredient_list': [

                ]
            }
            for elem in array_of_probe_details:
                if probe == elem['probe_number']:
                    temp_elem['probe_number'] = probe
                    temp_elem['trial_date'] = elem['date_of_trial']
                    temp_elem['water_category'] = elem['water_category']
                    # temp_elemp['company_name'] = elem['']
                    # print(elem)
                    if len(elem['key_params']) == 1:
                        temp_elem['ingredient_list'].append({
                            'ingredient_name': elem['ingredient_name'],
                            'first_param_name': elem['key_params']['first_param'],
                            'first_param_value': elem['key_params_values']['first_value'],
                        })
                    else:
                        temp_elem['ingredient_list'].append({
                            'ingredient_name': elem['ingredient_name'],
                            'first_param_name': elem['key_params']['first_param'],
                            'second_param_name': elem['key_params']['second_param'],
                            'first_param_value': elem['key_params_values']['first_value'],
                            'second_param_value': elem['key_params_values']['second_value'],
                        })
            probe_details_container.append(temp_elem)
            container.append(probe_details_container)
        return container

    def probes_digit_unification(self):
        probe_list = self.sorter(ExcelTabWriter)
        separated_probe = []
        multiple_probe = []
        unique_probes_first_index = []

        # Number of occurrences
        for probe in probe_list:
            for props in probe:
                probe_first_index = props['probe_number'].split('.')[0]
                unique_probes_first_index.append(probe_first_index)
        unique_probes_length = len(unique_probes_first_index)
        unique_probes_occurrence = {}
        # Unique probe occurrence counter
        for elem in range(0, unique_probes_length):
            counter = unique_probes_first_index.count(unique_probes_first_index[elem])
            unique_probes_occurrence[unique_probes_first_index[elem]] = counter

        for key, value in unique_probes_occurrence.items():
            counter = 0
            temp_array = []
            for probe in probe_list:
                if probe[0]['probe_number'].split('.')[0] == key:
                    counter += 1
                    # print(probe[0]['probe_number'], key, value)
                    if value == 1:
                        separated_probe.append(probe[0])
                    else:
                        # print(probe[0]['probe_number'], key, value)
                        temp_probe = probe[0]
                        temp_array.append(temp_probe)
                        # if len(multiple_probe) == 1:
                        #     multiple_probe.append(temp_array)
            if len(temp_array) != 0:
                multiple_probe.append(temp_array)

        return multiple_probe, separated_probe, probe_list

    def nd_searcher(self, ingredient_name, water_category):
        nd_tab = self.ingredient_tab
        # start_row = 3
        # start_cell = nd_tab.cell(3, 3).value
        max_row = nd_tab.max_row
        ingredient_nd_list = []
        # ingredient_list = []
        needed_pd = None

        for ingredient_row in range(3, max_row + 1):
            current_nd = nd_tab.cell(ingredient_row, 5).value
            current_ingredient_name = nd_tab.cell(ingredient_row, 3).value
            current_water_category = nd_tab.cell(ingredient_row, 6).value
            ingredient_nd_list.append([current_ingredient_name, current_nd, current_water_category])

        for ingredient in ingredient_nd_list:
            if ingredient[0] == ingredient_name and ingredient[2] is None:
                needed_pd = ingredient[1]
            elif ingredient[0] == ingredient_name and ingredient[2] == water_category:
                needed_pd = ingredient[1]

        return needed_pd

        # return ingredient_name

    def excel_writer(self):
        main_sheet = self.current_tab
        # ingredient_sheet = self.ingredient_tab
        multiple_probe = self.probes_digit_unification()[0]
        separated_probe = self.probes_digit_unification()[1]

        # Cells and their respective values
        probe_number_ex = main_sheet.cell(17, 9)  # Probe number at I17, where the protocol is
        probe_number_act = main_sheet.cell(28, 8)  # H28, same as above
        name_of_company = main_sheet.cell(20, 8)  # H20, a company's name
        water_category = main_sheet.cell(26, 8)  # H26, water's category
        trial_dial = main_sheet.cell(34, 8)  # H34, trial date
        protocol_info = main_sheet.cell(62, 1)  # Protocol from №/date

        for probe in separated_probe:
            # Here we write down all about one probe to a doc
            ingredient_list = probe['ingredient_list']
            probe_num = probe['probe_number']
            name_of_company.value = f"{company_name} ИНН:"
            probe_number_ex.value = probe_num
            probe_number_act.value = probe_num
            water_category.value = probe['water_category']
            trial_dial.value = probe['trial_date']
            protocol_info.value = \
                f"Протокол № f{probe_num} от 'УКАЗАТЬ ДАТУ' г. на 'УКАЗАТЬ КОЛ_ВО СТРАНИЦ' страницах"

            start_row = 66
            probe_column = 1
            ingredient_column = 5
            measurement_column = 11
            results_column = 12
            nds_column = 15

            start_cell = main_sheet.cell(start_row, probe_column)
            start_cell.value = probe_num

            for ingredient in ingredient_list:
                # Appending an ingredient name to "Определяемый показатель" row
                temp_ingredient_name = ingredient['ingredient_name']
                temp_ingredient_name_cell = main_sheet.cell(start_row, ingredient_column)
                # temp_ingredient_name_cell.value = temp_ingredient_name
                temp_nd = main_sheet.cell(start_row, nds_column)

                ############
                # temp_ingredient_name_cell.value = temp_ingredient_name
                # ingredient_cell_value = temp_ingredient_name_cell.value

                # Appending ingredient names
                if temp_ingredient_name == 'Аммоний-ион 50 (питьевая)' \
                        or temp_ingredient_name == 'Аммоний-ион 10 (питьевая)' \
                        or temp_ingredient_name == 'Аммоний-ион 50 (природная и сточная)' \
                        or temp_ingredient_name == 'Аммоний-ион 10 (природная и сточная)':
                    temp_ingredient_name_cell.value = 'Аммоний-ион'
                elif temp_ingredient_name == 'АПАВ':
                    temp_ingredient_name_cell.value = 'Анионные поверхностно-активные вещества (АПАВ)'
                elif temp_ingredient_name == 'БПК':
                    temp_ingredient_name_cell.value = 'Биохимическое потребление кислорода (БПК n)'
                elif temp_ingredient_name == 'рН':
                    temp_ingredient_name_cell.value = 'Водородный показатель (рН)'
                elif temp_ingredient_name == 'Жесткость(питьевая)':
                    temp_ingredient_name_cell.value = 'Жесткость'
                elif temp_ingredient_name == 'Общая жесткость (природная, сточная)':
                    temp_ingredient_name_cell.value = 'Общая жесткость '
                elif temp_ingredient_name == 'Сульфат-ион (природная, сточная)' \
                        or temp_ingredient_name == 'Сульфат-ион (природная, сточная)':
                    temp_ingredient_name_cell.value = 'Сульфат-ион'
                elif temp_ingredient_name == 'Хлориды (питьевая)' \
                        or temp_ingredient_name == 'Хлориды (природная, сточная)':
                    temp_ingredient_name_cell.value = 'Хлориды'
                elif temp_ingredient_name == 'Фосфаты (50мм)' \
                        or temp_ingredient_name == 'Фосфаты (20мм)':
                    temp_ingredient_name_cell.value = 'Фосфат-ион'
                elif temp_ingredient_name == 'Нитрит-ион ':
                    temp_ingredient_name_cell.value = 'мг/дм³'
                elif temp_ingredient_name == 'Железо общее 50' \
                        or temp_ingredient_name == 'Железо общее 10':
                    temp_ingredient_name_cell.value = 'Железо общее'
                elif temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)' \
                        or temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)':
                    temp_ingredient_name_cell.value = 'Гидрокарбонаты'
                elif temp_ingredient_name == 'Нитраты (питьевая, природная)' \
                        or temp_ingredient_name == 'Нитраты (сточная)':
                    temp_ingredient_name_cell.value = 'Нитрат-ион'
                elif temp_ingredient_name == 'Фториды(питьевая,природная)' \
                        or temp_ingredient_name == 'Фториды(питьевая,природная)':
                    temp_ingredient_name_cell.value = 'Фториды'
                elif temp_ingredient_name == 'Хром VI 50' \
                        or temp_ingredient_name == 'Хром VI 10':
                    temp_ingredient_name_cell.value = 'Хром (VI)'
                elif temp_ingredient_name == 'НП (сточная)' \
                        or temp_ingredient_name == 'НП (питьевая, природная,очищ. сточная)':
                    temp_ingredient_name_cell.value = 'Нефтепродукты'
                elif temp_ingredient_name == 'ПО':
                    temp_ingredient_name_cell.value = 'Перманганатная окисляемость'
                elif temp_ingredient_name == 'ХПК':
                    temp_ingredient_name_cell.value = 'Химическое потребление кислорода (ХПК)'
                elif temp_ingredient_name == 'Общий хлор':
                    temp_ingredient_name_cell.value = 'Общий хлор (остаточный активный)'
                else:
                    temp_ingredient_name_cell.value = temp_ingredient_name

                # Appending nds
                if temp_ingredient_name == 'Аммоний-ион 50 (питьевая)' \
                        or temp_ingredient_name == 'Аммоний-ион 10 (питьевая)' \
                        or temp_ingredient_name == 'Аммоний-ион 50 (природная и сточная)' \
                        or temp_ingredient_name == 'Аммоний-ион 10 (природная и сточная)':
                    temp_nd.value = 'ПНД Ф 14.1:2:4.262-10 (ФР.1.31.2010.07603)'
                elif temp_ingredient_name == 'НП (сточная)':
                    temp_nd.value = 'ПНД Ф 14.1:272-2012 (ФР.1.31.2017.26179)'
                elif temp_ingredient_name == 'НП (питьевая, природная,очищ. сточная)':
                    temp_nd.value = 'ПНД Ф 14.1:2:4.168-2000 (ФР.1.31.2017.26183)'
                elif temp_ingredient_name == 'Сульфат-ион (питьевая)':
                    temp_nd.value = 'ГОСТ 31940-2012 (метод 2)'
                elif temp_ingredient_name == 'Сульфат-ион (природная, сточная)':
                    temp_nd.value = 'ПНД Ф 14.1:2.159-2000'
                elif temp_ingredient_name == 'Хлориды (природная, сточная)':
                    temp_nd.value = 'ПНД Ф 14.1:2:3.96-97'
                elif temp_ingredient_name == 'Хлориды (питьевая)':
                    temp_nd.value = 'ГОСТ 4245-72 (п.2)'
                elif temp_ingredient_name == 'Фосфаты (50мм)' \
                        or temp_ingredient_name == 'Фосфаты (20мм)':
                    temp_nd.value = 'ПНД Ф 14.1:2:4.112-97'
                elif temp_ingredient_name == 'Железо общее 50' \
                        or temp_ingredient_name == 'Железо общее 10':
                    temp_nd.value = 'ПНД Ф 14.1:2:4.50-96'
                elif temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)' \
                        or temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)':
                    temp_nd.value = 'ПНД Ф 14.1:2:3.99-97 (обратное титрование)'
                elif temp_ingredient_name == 'Нитраты (питьевая, природная)' \
                        or temp_ingredient_name == 'Нитраты (сточная)':
                    temp_nd.value = 'ПНД Ф 14.1:2:4.4-95'
                elif temp_ingredient_name == 'Фториды(питьевая,природная)' \
                        or temp_ingredient_name == 'Фториды(питьевая,природная)':
                    temp_nd.value = 'ПНД Ф 14.1:2:4.270-2012'
                elif temp_ingredient_name == 'Хром VI 50' \
                        or temp_ingredient_name == 'Хром VI 10':
                    temp_nd.value = 'ПНД Ф 14.1:2:4.52-96'
                elif temp_ingredient_name == 'Прозрачность':
                    temp_nd.value = 'РД 52.24.496-2018'
                else:
                    nd_searcher = self.nd_searcher(temp_ingredient_name, water_category.value)
                    temp_nd.value = nd_searcher

                try:
                    # temp_first_name = ingredient['first_param_name']
                    temp_first_value = ingredient['first_param_value']
                    # temp_second_name = ingredient['second_param_name']
                    temp_second_value = ingredient['second_param_value']

                    # Appending measurement
                    #######
                    measure_cell = main_sheet.cell(start_row, measurement_column)
                    if temp_ingredient_name == 'Аммоний-ион 50 (питьевая)' \
                            or temp_ingredient_name == 'Аммоний-ион 10 (питьевая)' \
                            or temp_ingredient_name == 'Аммоний-ион 50 (природная и сточная)' \
                            or temp_ingredient_name == 'Аммоний-ион 10 (природная и сточная)':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'АПАВ':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'БПК':
                        measure_cell.value = 'мг О2/дм³³'
                    elif temp_ingredient_name == 'рН':
                        measure_cell.value = 'ед. рН'
                    elif temp_ingredient_name == 'Жесткость(питьевая)':
                        measure_cell.value = '°Ж'
                    elif temp_ingredient_name == 'Общая жесткость (природная, сточная)':
                        measure_cell.value = '°Ж'
                    elif temp_ingredient_name == 'Сульфат-ион (природная, сточная)' \
                            or temp_ingredient_name == 'Сульфат-ион (природная, сточная)':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'Хлориды (питьевая)' \
                            or temp_ingredient_name == 'Хлориды (природная, сточная)':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'Фосфаты (50мм)' \
                            or temp_ingredient_name == 'Фосфаты (20мм)':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'Железо общее 50' \
                            or temp_ingredient_name == 'Железо общее 10':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)' \
                            or temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'Нитраты (питьевая, природная)' \
                            or temp_ingredient_name == 'Нитраты (сточная)':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'Фториды(питьевая,природная)' \
                            or temp_ingredient_name == 'Фториды(питьевая,природная)':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'Хром VI 50' \
                            or temp_ingredient_name == 'Хром VI 10':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'НП (сточная)' \
                            or temp_ingredient_name == 'НП (питьевая, природная,очищ. сточная)':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'ПО':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'ХПК':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'Общий хлор':
                        measure_cell.value = 'мг/дм³'
                    elif temp_ingredient_name == 'Мутность':
                        measure_cell.value = 'ЕМФ'
                    elif temp_ingredient_name == 'Цветность':
                        measure_cell.value = '° цветности'
                    elif temp_ingredient_name == 'Общая щелочность':
                        measure_cell.value = 'ммоль/дм3'
                    elif temp_ingredient_name == 'Свободная щелочность':
                        measure_cell.value = 'ммоль/дм3'
                    elif temp_ingredient_name == 'Цвет':
                        measure_cell.value = '-'
                    elif temp_ingredient_name == 'Прозрачность':
                        measure_cell.value = 'см'
                    elif temp_ingredient_name == 'Температура':
                        measure_cell.value = '°С'
                    elif temp_ingredient_name == 'Запах при 20°С' \
                            or temp_ingredient_name == 'Запах при 60°С' \
                            or temp_ingredient_name == 'Запах при 60°' \
                            or temp_ingredient_name == 'Запах при 20°':
                        measure_cell.value = 'баллы'
                    elif temp_ingredient_name == 'Вкус':
                        measure_cell.value = 'баллы'
                    elif temp_ingredient_name == 'Алюминий' \
                            or temp_ingredient_name == 'Взвешенные вещества' \
                            or temp_ingredient_name == 'Жиры' \
                            or temp_ingredient_name == 'Кальций' \
                            or temp_ingredient_name == 'Марганец' \
                            or temp_ingredient_name == 'Нитрит-ион' \
                            or temp_ingredient_name == 'Нитраты (питьевая, природная)' \
                            or temp_ingredient_name == 'Нитраты (сточная)' \
                            or temp_ingredient_name == 'Сухой остаток' \
                            or temp_ingredient_name == 'Растворенный кислород':
                        measure_cell.value = 'мг/дм³'
                    #######

                    # Appending results themselves
                    results_cell = main_sheet.cell(start_row, results_column)
                    if temp_second_value == "нет" or temp_second_value == "-":
                        results_cell.value = temp_first_value
                    else:
                        # temp_first_value = round(temp_first_value, 2)
                        # temp_second_value = round(temp_second_value, 2)
                        temp_second_value = '{0:.2f}'.format(temp_second_value).rstrip('0').rstrip('.')
                        temp_first_value = '{0:.2f}'.format(temp_first_value).rstrip('0').rstrip('.')
                        results_cell.value = f"{temp_first_value}±{temp_second_value}"

                    # Searching for a ND
                except KeyError:
                    temp_first_name = ingredient['first_param_name']
                    temp_first_value = ingredient['first_param_value']

                    # Appending measurement
                    measure_cell = main_sheet.cell(start_row, measurement_column)
                    if len(str(temp_first_name).split(',')) == 1:
                        measure_cell.value = temp_first_name
                    else:
                        if temp_first_name.split(',')[1] == ' С мг/дм3':
                            measure_cell.value = 'мг/дм3'
                        else:
                            measure_cell.value = temp_first_name.split(',')[1]
                    measure_cell = main_sheet.cell(start_row, measurement_column)
                    measure_cell.value = temp_first_name

                    # Appending results themselves
                    results_cell = main_sheet.cell(start_row, results_column)
                    results_cell.value = f"{temp_first_value}"

                start_row += 1

                destination_filename = f'./exporter_dir/Протокол вод ({probe["probe_number"]}).xlsx'
                self.current_doc.save(filename=destination_filename)

        for probe_list in multiple_probe:
            # Here we write down all about one probe to a doc
            probe_name_doc = probe_list[0]['probe_number'][:-2]
            start_row = 66
            for probe_elem in range(0, len(probe_list)):
                probe = probe_list[probe_elem]
                ingredient_list = probe['ingredient_list']
                probe_num = probe['probe_number']
                name_of_company.value = f"{company_name} ИНН:"
                probe_number_ex.value = probe_num[:-2]
                probe_number_act.value = probe_num[:-2]
                water_category.value = probe['water_category']
                trial_dial.value = probe['trial_date']
                protocol_info.value = \
                    f"Протокол № f{probe_num} от 'УКАЗАТЬ ДАТУ' г. на 'УКАЗАТЬ КОЛ_ВО СТРАНИЦ' страницах"
                probe_column = 1
                ingredient_column = 5
                measurement_column = 11
                results_column = 12
                nds_column = 15

                start_cell = main_sheet.cell(start_row, probe_column)
                start_cell.value = probe_num

                for ingredient in ingredient_list:
                    # Appending an ingredient name to "Определяемый показатель" row
                    temp_ingredient_name = ingredient['ingredient_name']
                    temp_ingredient_name_cell = main_sheet.cell(start_row, ingredient_column)
                    temp_ingredient_name_cell.value = temp_ingredient_name
                    temp_nd = main_sheet.cell(start_row, nds_column)

                    # Appending ingredient names
                    if temp_ingredient_name == 'Аммоний-ион 50 (питьевая)' \
                            or temp_ingredient_name == 'Аммоний-ион 10 (питьевая)' \
                            or temp_ingredient_name == 'Аммоний-ион 50 (природная и сточная)' \
                            or temp_ingredient_name == 'Аммоний-ион 10 (природная и сточная)':
                        temp_ingredient_name_cell.value = 'Аммоний-ион'
                    elif temp_ingredient_name == 'АПАВ':
                        temp_ingredient_name_cell.value = 'Анионные поверхностно-активные вещества (АПАВ)'
                    elif temp_ingredient_name == 'БПК':
                        temp_ingredient_name_cell.value = 'Биохимическое потребление кислорода (БПК n)'
                    elif temp_ingredient_name == 'рН':
                        temp_ingredient_name_cell.value = 'Водородный показатель (рН)'
                    elif temp_ingredient_name == 'Жесткость(питьевая)':
                        temp_ingredient_name_cell.value = 'Жесткость'
                    elif temp_ingredient_name == 'Общая жесткость (природная, сточная)':
                        temp_ingredient_name_cell.value = 'Общая жесткость '
                    elif temp_ingredient_name == 'Сульфат-ион (природная, сточная)' \
                            or temp_ingredient_name == 'Сульфат-ион (природная, сточная)':
                        temp_ingredient_name_cell.value = 'Сульфат-ион'
                    elif temp_ingredient_name == 'Хлориды (питьевая)' \
                            or temp_ingredient_name == 'Хлориды (природная, сточная)':
                        temp_ingredient_name_cell.value = 'Хлориды'
                    elif temp_ingredient_name == 'Фосфаты (50мм)' \
                            or temp_ingredient_name == 'Фосфаты (20мм)':
                        temp_ingredient_name_cell.value = 'Фосфат-ион'
                    elif temp_ingredient_name == 'Железо общее 50' \
                            or temp_ingredient_name == 'Железо общее 10':
                        temp_ingredient_name_cell.value = 'Железо общее'
                    elif temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)' \
                            or temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)':
                        temp_ingredient_name_cell.value = 'Гидрокарбонаты'
                    elif temp_ingredient_name == 'Нитраты (питьевая, природная)' \
                            or temp_ingredient_name == 'Нитраты (сточная)':
                        temp_ingredient_name_cell.value = 'Нитрат-ион'
                    elif temp_ingredient_name == 'Фториды(питьевая,природная)' \
                            or temp_ingredient_name == 'Фториды(питьевая,природная)':
                        temp_ingredient_name_cell.value = 'Фториды'
                    elif temp_ingredient_name == 'Хром VI 50' \
                            or temp_ingredient_name == 'Хром VI 10':
                        temp_ingredient_name_cell.value = 'Хром (VI)'
                    elif temp_ingredient_name == 'НП (сточная)' \
                            or temp_ingredient_name == 'НП (питьевая, природная,очищ. сточная)':
                        temp_ingredient_name_cell.value = 'Нефтепродукты'
                    elif temp_ingredient_name == 'ПО':
                        temp_ingredient_name_cell.value = 'Перманганатная окисляемость'
                    elif temp_ingredient_name == 'ХПК':
                        temp_ingredient_name_cell.value = 'Химическое потребление кислорода (ХПК)'
                    elif temp_ingredient_name == 'Общий хлор':
                        temp_ingredient_name_cell.value = 'Общий хлор (остаточный активный)'
                    else:
                        temp_ingredient_name_cell.value = temp_ingredient_name

                    # Appending nds
                    if temp_ingredient_name == 'Аммоний-ион 50 (питьевая)' \
                            or temp_ingredient_name == 'Аммоний-ион 10 (питьевая)' \
                            or temp_ingredient_name == 'Аммоний-ион 50 (природная и сточная)' \
                            or temp_ingredient_name == 'Аммоний-ион 10 (природная и сточная)':
                        temp_nd.value = 'ПНД Ф 14.1:2:4.262-10 (ФР.1.31.2010.07603)'
                    elif temp_ingredient_name == 'НП (сточная)':
                        temp_nd.value = 'ПНД Ф 14.1:272-2012 (ФР.1.31.2017.26179)'
                    elif temp_ingredient_name == 'НП (питьевая, природная,очищ. сточная)':
                        temp_nd.value = 'ПНД Ф 14.1:2:4.168-2000 (ФР.1.31.2017.26183)'
                    elif temp_ingredient_name == 'Сульфат-ион (питьевая)':
                        temp_nd.value = 'ГОСТ 31940-2012 (метод 2)'
                    elif temp_ingredient_name == 'Сульфат-ион (природная, сточная)':
                        temp_nd.value = 'ПНД Ф 14.1:2.159-2000'
                    elif temp_ingredient_name == 'Хлориды (природная, сточная)':
                        temp_nd.value = 'ПНД Ф 14.1:2:3.96-97'
                    elif temp_ingredient_name == 'Хлориды (питьевая)':
                        temp_nd.value = 'ГОСТ 4245-72 (п.2)'
                    elif temp_ingredient_name == 'Фосфаты (50мм)' \
                            or temp_ingredient_name == 'Фосфаты (20мм)':
                        temp_nd.value = 'ПНД Ф 14.1:2:4.112-97'
                    elif temp_ingredient_name == 'Железо общее 50' \
                            or temp_ingredient_name == 'Железо общее 10':
                        temp_nd.value = 'ПНД Ф 14.1:2:4.50-96'
                    elif temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)' \
                            or temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)':
                        temp_nd.value = 'ПНД Ф 14.1:2:3.99-97 (обратное титрование)'
                    elif temp_ingredient_name == 'Нитраты (питьевая, природная)' \
                            or temp_ingredient_name == 'Нитраты (сточная)':
                        temp_nd.value = 'ПНД Ф 14.1:2:4.4-95'
                    elif temp_ingredient_name == 'Фториды(питьевая,природная)' \
                            or temp_ingredient_name == 'Фториды(питьевая,природная)':
                        temp_nd.value = 'ПНД Ф 14.1:2:4.270-2012'
                    elif temp_ingredient_name == 'Хром VI 50' \
                            or temp_ingredient_name == 'Хром VI 10':
                        temp_nd.value = 'ПНД Ф 14.1:2:4.52-96'
                    elif temp_ingredient_name == 'Прозрачность':
                        temp_nd.value = 'РД 52.24.496-2018'
                    else:
                        nd_searcher = self.nd_searcher(temp_ingredient_name, water_category.value)
                        temp_nd.value = nd_searcher

                    try:
                        # temp_first_name = ingredient['first_param_name']
                        temp_first_value = ingredient['first_param_value']
                        # temp_second_name = ingredient['second_param_name']
                        temp_second_value = ingredient['second_param_value']

                        # print(f"First Value:{temp_first_value} \n "
                        #       f"First Value Type:{type(temp_first_value)} \n"
                        #       f"Second Value:{temp_second_value} \n "
                        #       f"Second Value Type:{type(temp_second_value)} \n"
                        #       )

                        # Appending measurement
                        # measure_cell = main_sheet.cell(start_row, measurement_column)
                        # if len(str(temp_first_name).split(',')) == 1:
                        #     measure_cell.value = temp_first_name
                        # else:
                        #     if temp_first_name.split(',')[1] == ' С мг/дм3':
                        #         measure_cell.value = 'мг/дм3'
                        #     else:
                        #         measure_cell.value = temp_first_name.split(',')[1]

                        measure_cell = main_sheet.cell(start_row, measurement_column)
                        if temp_ingredient_name == 'Аммоний-ион 50 (питьевая)' \
                                or temp_ingredient_name == 'Аммоний-ион 10 (питьевая)' \
                                or temp_ingredient_name == 'Аммоний-ион 50 (природная и сточная)' \
                                or temp_ingredient_name == 'Аммоний-ион 10 (природная и сточная)':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'АПАВ':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'БПК':
                            measure_cell.value = 'мг О2/дм³³'
                        elif temp_ingredient_name == 'рН':
                            measure_cell.value = 'ед. рН'
                        elif temp_ingredient_name == 'Жесткость(питьевая)':
                            measure_cell.value = '°Ж'
                        elif temp_ingredient_name == 'Общая жесткость (природная, сточная)':
                            measure_cell.value = '°Ж'
                        elif temp_ingredient_name == 'Сульфат-ион (природная, сточная)' \
                                or temp_ingredient_name == 'Сульфат-ион (природная, сточная)':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Хлориды (питьевая)' \
                                or temp_ingredient_name == 'Хлориды (природная, сточная)':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Фосфаты (50мм)' \
                                or temp_ingredient_name == 'Фосфаты (20мм)':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Железо общее 50' \
                                or temp_ingredient_name == 'Железо общее 10':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)' \
                                or temp_ingredient_name == 'Гидрокарбонаты (без окраски ф/ф)':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Нитрит-ион ':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Нитраты (питьевая, природная)' \
                                or temp_ingredient_name == 'Нитраты (сточная)':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Фториды(питьевая,природная)' \
                                or temp_ingredient_name == 'Фториды(питьевая,природная)':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Хром VI 50' \
                                or temp_ingredient_name == 'Хром VI 10':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'НП (сточная)' \
                                or temp_ingredient_name == 'НП (питьевая, природная,очищ. сточная)':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'ПО':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'ХПК':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Общий хлор':
                            measure_cell.value = 'мг/дм³'
                        elif temp_ingredient_name == 'Мутность':
                            measure_cell.value = 'ЕМФ'
                        elif temp_ingredient_name == 'Цветность':
                            measure_cell.value = '° цветности'
                        elif temp_ingredient_name == 'Общая щелочность':
                            measure_cell.value = 'ммоль/дм3'
                        elif temp_ingredient_name == 'Свободная щелочность':
                            measure_cell.value = 'ммоль/дм3'
                        elif temp_ingredient_name == 'Цвет':
                            measure_cell.value = '-'
                        elif temp_ingredient_name == 'Прозрачность':
                            measure_cell.value = 'см'
                        elif temp_ingredient_name == 'Вкус':
                            measure_cell.value = 'баллы'
                        elif temp_ingredient_name == 'Температура':
                            measure_cell.value = '°С'
                        elif temp_ingredient_name == 'Запах при 20°С' \
                                or temp_ingredient_name == 'Запах при 60°С' \
                                or temp_ingredient_name == 'Запах при 60°' \
                                or temp_ingredient_name == 'Запах при 20°':
                            measure_cell.value = 'баллы'
                        elif temp_ingredient_name == 'Алюминий' \
                                or temp_ingredient_name == 'Взвешенные вещества' \
                                or temp_ingredient_name == 'Жиры' \
                                or temp_ingredient_name == 'Кальций' \
                                or temp_ingredient_name == 'Марганец' \
                                or temp_ingredient_name == 'Нитрит-ион' \
                                or temp_ingredient_name == 'Нитраты (питьевая, природная)' \
                                or temp_ingredient_name == 'Нитраты (сточная)' \
                                or temp_ingredient_name == 'Сухой остаток' \
                                or temp_ingredient_name == 'Растворенный кислород':
                            measure_cell.value = 'мг/дм³'

                        # Appending results themselves
                        results_cell = main_sheet.cell(start_row, results_column)
                        if temp_second_value == "нет" or temp_second_value == "-":
                            results_cell.value = temp_first_value
                        else:
                            temp_first_value = round(temp_first_value, 2)
                            if type(temp_second_value) == str:
                                results_cell.value = f"{temp_first_value}"
                            else:
                                temp_second_value = '{0:.2f}'.format(temp_second_value).rstrip('0').rstrip('.')
                                temp_first_value = '{0:.2f}'.format(temp_first_value).rstrip('0').rstrip('.')
                                results_cell.value = f"{temp_first_value}±{temp_second_value}"
                                # temp_second_value = round(temp_second_value, 2)
                                # results_cell.value = f"{temp_first_value}±{temp_second_value}"

                        # Searching for a ND
                    except KeyError:
                        temp_first_name = ingredient['first_param_name']
                        temp_first_value = ingredient['first_param_value']

                        # Appending measurement
                        measure_cell = main_sheet.cell(start_row, measurement_column)
                        if len(str(temp_first_name).split(',')) == 1:
                            measure_cell.value = temp_first_name
                        else:
                            if temp_first_name.split(',')[1] == ' С мг/дм3':
                                measure_cell.value = 'мг/дм3'
                            else:
                                measure_cell.value = temp_first_name.split(',')[1]
                        measure_cell = main_sheet.cell(start_row, measurement_column)
                        measure_cell.value = temp_first_name

                        # Appending results themselves
                        results_cell = main_sheet.cell(start_row, results_column)
                        results_cell.value = f"{temp_first_value}"

                    start_row += 1
            destination_filename = f'./exporter_dir/Протокол вод ({probe_name_doc}).xlsx'
            self.current_doc.save(filename=destination_filename)


fun_exec = ExcelTabWriter()
fun_exec.excel_writer()
